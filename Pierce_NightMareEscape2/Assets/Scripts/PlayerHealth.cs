﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 100;
    public float currentHealth;
    public static PlayerHealth singleton;
    public bool isDead = false;

    public HealthBar healthBar;
    // Start is called before the first frame update
    void Awake()
    {
        singleton = this;
     
    }
    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlayerDamage(20);
        }
    }

        public void PlayerDamage(float damage)
        {
            if(currentHealth > 0)
        { 

            currentHealth -= damage;

        }
            else

        {
            Dead();
        }
    }
    void Dead()
    {
        currentHealth = 0;
        isDead = true;
        Debug.Log("Player Is Dead");
    }

}