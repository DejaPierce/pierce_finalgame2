﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour
{
    // Start is called before the first frame update
    public float damage = 10f;
    public float range = 100f;

    public int maxAmmo = 10;
    private int currentAmmo;
    public float reloadTime = 1f;
    private bool isReloading = false;

    public Camera fpsCam;

    public Animator animator;

    public ParticleSystem muzzleflash;


    AudioSource gunAS;
    public AudioClip shootAC;

    // Update is called once per frame

    void Start()
    {
        if (currentAmmo == -1)
        currentAmmo = maxAmmo;
        muzzleflash.Stop();
        gunAS = GetComponent<AudioSource>();

    }

    private void OnEnable()
    {
        isReloading = false;
        animator.SetBool("Reloading", false);
    }
    void Update()
    {
        if (isReloading)
            return;
        if (currentAmmo <= 0)
        {
            StartCoroutine(Reload());
            return;
        }
       if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    IEnumerator Reload ()
    {
        isReloading = true;
        Debug.Log("Reloading...");

        animator.SetBool("Reloading", true);

        yield return new WaitForSeconds(reloadTime - .25f);

        animator.SetBool("Reloading", false);

        yield return new WaitForSeconds(.25f);

        currentAmmo = maxAmmo;
        isReloading = false;
    }

    IEnumerator WeaponEffects()
    {
        muzzleflash.Play();
        yield return new WaitForEndOfFrame();
        muzzleflash.Stop();
    }

    void Shoot()
    {
        currentAmmo--;

        StartCoroutine(WeaponEffects());
        RaycastHit hit;
        gunAS.PlayOneShot(shootAC);
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range));
        {
    
            Debug.Log(hit.transform.name);

            Enemy enemy = hit.transform.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.DeductHealth(damage);
            }
        }
    }
}
