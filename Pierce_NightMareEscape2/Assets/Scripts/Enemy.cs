﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    public float enemyHealth = 50f;
    EnemyAI enemyAI;

    private void Start()
    {
        enemyAI = GetComponent<EnemyAI>();
    }

    public void DeductHealth (float deductHealth)
    {
        enemyHealth -= deductHealth;
            if (enemyHealth <= 0) { EnemyDead(); }
   
    }
    void EnemyDead()
    {
        enemyAI.EnemyDeathAnim();
        UIManager.instance.killCount++;
        UIManager.instance.UpdateKillCounterUI();
        Destroy(gameObject,10);
    }
}
